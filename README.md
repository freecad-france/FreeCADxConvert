# FreeCADxConvert
Conversion modèles 3d en ligne.
Visualisation de modèles 3d en ligne.

## Format compatibles
### Solide
- STEP
- IGES
- BREP

### Polygonale
- STL
- OBJ
- AST
- DAE

### Autre
- IFC


## Installation Ubuntu 16.04

	sudo add-apt-repository ppa:freecad-maintainers/freecad-daily
	#Appuyer sur Entrée quand demandé
	sudo apt-get update
	sudo apt-get install git freecad python-django
	mkdir DjangoProjects
	cd DjangoProjects
	#Si vous voulez vous connecter au dépôt par HTTPS
	git clone https://framagit.org/freecad-france/FreeCADxConvert.git
	#Si vous voulez vous connecter au dépôt par SSH
	git clone git@framagit.org:freecad-france/FreeCADxConvert.git
	cd FreeCADxConvert
    python manage.py migrate
    python manage.py runserver 0.0.0.0:9540
	
Maintenant vous devriez pouvoir accéder à l'instance à cette adresse

http://localhost:9540

## Utilisation
Index : Choisir un fichier 3d sur votre ordinateur et appuyer sur le bouton Open.
Preview : Vous pouvez naviguer dans la vue 3d de l'objet
Clic gauche : rotation
Clic molette : Zoom

Vous pouvez choisir le format d'export dans la liste puis cliquer sur Exporter.
