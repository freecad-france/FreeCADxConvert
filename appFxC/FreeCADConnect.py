# -*- coding: utf-8 -*-

#***************************************************************************
#*                                                                         *
#*   Copyright (c) 2016                                                    *
#*   Jonathan Wiedemann <contact@freecad-france.com>                       *
#*                                                                         *
#*   This program is free software; you can redistribute it and/or modify  *
#*   it under the terms of the GNU Lesser General Public License (LGPL)    *
#*   as published by the Free Software Foundation; either version 2 of     *
#*   the License, or (at your option) any later version.                   *
#*   for detail see the LICENCE text file.                                 *
#*                                                                         *
#*   This program is distributed in the hope that it will be useful,       *
#*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
#*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
#*   GNU Library General Public License for more details.                  *
#*                                                                         *
#*   You should have received a copy of the GNU Library General Public     *
#*   License along with this program; if not, write to the Free Software   *
#*   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
#*   USA                                                                   *
#*                                                                         *
#***************************************************************************

__title__="FreeCADxConvert"
__author__ = "Jonathan Wiedemann"
__url__ = "https://www.freecad-france.com"


import sys
FREECADPATH='/usr/lib/freecad/lib'
sys.path.append(FREECADPATH)
import FreeCAD, Draft, Part, DraftGeomUtils
FreeCADGui = None

DEBUG = True

def getFreeCADVersion() :
    """get the current version of FreeCAD"""
    fcversion = FreeCAD.Version()[ 0 : 3 ]
    fcversion = str( fcversion[0] ) + "." + str( fcversion[1] ) + "." + str( fcversion[2] )
    if DEBUG:
        print( 'FreeCADConnect.getFreeCADVersion report ...' )
        print( '     return', fcversion)
        print( '... end FreeCADConnect.getFreeCADVersion .' )
    return fcversion

def load3dfile( path, label ) :
    """load a file in FreeCAD and return its freecad document name"""
    labelsplit = label.rsplit( '.' , 1 )
    fileextension = labelsplit[1].lower()
    doc = FreeCAD.newDocument( labelsplit[0] )
    filename = doc.Name
    doc.Label = label
    
    if fileextension in [ "fcstd", ] :
        doc.load( path )
    
    elif fileextension in [ "step", "stp", "iges", "igs", "brp", "brep" ] :
        import Part
        Part.insert( path, filename )
        
    elif fileextension in [ "stl", "obj", "bms", "off", "ply", "ast"  ] :
        import Mesh
        Mesh.insert( path, filename )
        
    elif fileextension in [ "dae" ] :
        import importDAE
        importDAE.insert(path, filename )
        
    elif fileextension in [ "ifc" ] :
        ## Need to make some change in ArchMaterial.py in freecad code.
        ## Add some if FreeCADGui up.
        import importIFC
        importIFC.insert(path, filename )
    
    else :
        pass
    
    if DEBUG:
        print( 'FreeCADConnect.load3dfile report ...' )
        print( '    path was', path )
        print( '    label was', label )
        print( '    FC Doc instance', doc )
        print( '    FC Doc Name', doc.Name )
        print( '    FC Doc Label', doc.Label )
        print( '    FC Doc FileName', doc.FileName )
        print( '... end FreeCADConnect.load3dfile.' )
    
    return doc.Name

def getInstanceDoc( name ):
    """get an existing FreeCAD Document by its name"""
    doc = FreeCAD.getDocument( name )
    if DEBUG:
        print( 'FreeCADConnect.getInstanceDoc report ...' )
        print( '     name was', name ) 
        print( '     return', doc )
        print( '... end FreeCADConnect.getInstanceDoc.' )
    return doc

def export3dfile( name, extension, path=None ):
    """export the given FreeCAD doc to the format given by extension"""
    doc = getInstanceDoc( name )
    if extension in [ 'WEBGL' ] :
        return getThreeJS( doc )
        
    elif extension in [ 'STL', 'OBJ', 'AST', 'PLY', 'BMS', 'OFF' ] :
        return getMESH( doc, path, extension )
        
    elif extension in [ 'STEP', 'BREP', 'IGES' ] :
        return getPART( doc, path, extension )
        
    elif extension in [ 'DAE' ] :
        return getDAE( doc, path, extension )
        
    elif extension in [ 'IFC', ] :
        return getIFC( doc, path, extension )
        
    elif extension in [ 'FCSTD', ] :
        return getFCSTD( doc, path, extension )
        
def getMESH(doc, path, extension):
    """MESH export (stl, ast, obj, ply, bms) for the given doc"""
    objectsList = getVisibleObjects( doc )
    filepath = path + '.' + extension.lower()
    import Mesh
    Mesh.export( objectsList,filepath )
    if DEBUG:
        print( "FreeCADConnect.getSTL report ..." )
        print( "     getSTL filepath :" + filepath )
        print( "... FreeCADConnect.getSTL end." )
    return filepath


def getIFC( doc, path, extension ):
    """IFC export ( ifc ) for the given doc"""
    objectsList = getVisibleObjects( doc )
    filepath = path + '.' + extension.lower()
    import importIFC
    importIFC.export( objectsList,filepath )
    if DEBUG:
        print( "FreeCADConnect.getIFC report ..." )
        print( "     getIFC filepath :" + filepath )
        print( "... FreeCADConnect.getIFC end." )
    return filepath
    
def getPART( doc, path, extension ):
    """PART export (step, iges, brep) for the given doc"""
    objectsList = getVisibleObjects( doc )
    filepath = path + '.' + extension.lower()
    import Part
    partsList = []
    for obj in objectsList:
        if obj.isDerivedFrom("Part::Feature") :
            partsList.append(obj)
        elif obj.isDerivedFrom( "Mesh::Feature" ) :
            newobj = doc.addObject( "Part::Feature" , obj.Name )
            __shape__ = Part.Shape()
            __shape__.makeShapeFromMesh( obj.Mesh.Topology , 0.100000 )
            __shape__ = Part.Solid(__shape__)
            newobj.Shape = __shape__.removeSplitter()
            newobj.purgeTouched()
            del __shape__
            partsList.append(newobj)
    Part.export( partsList, filepath )
    if DEBUG:
        print( "FreeCADConnect.getSTEP report ..." )
        print( "     getPART filepath :" + filepath )
        print( "... FreeCADConnect.getSTEP end." )
    return filepath

def getDAE( doc, path, extension  ):
    filepath = path + '.dae'
    objectsList = getVisibleObjects( doc )
    import importDAE
    importDAE.export(objectsList,filepath)
    return filepath

def getFCSTD( doc, path, extension  ) :
    filepath = path + '.fcstd'
    doc.saveAs( filepath )
    return filepath

def closeDocument(label):
        FreeCAD.closeDocument(self.doc.Name)

def getVisibleObjects(doc):
    if DEBUG :
        print( 'FreeCADConnect.getVisibleObjects report ...' )
        print( '    len(doc.Objects) : ', len(doc.Objects) )
    if not doc.FileName :
        visibleObjectList = doc.Objects
        if DEBUG :
            print( "    len(visibleObjectList) : ", len(visibleObjectList) )
        return visibleObjectList
    else :
        import zipfile
        fcstdfile = zipfile.ZipFile(doc.FileName)
        visibleObjectNameList = []
        visibleObjectList = []
        if 'GuiDocument.xml' in fcstdfile.namelist():
            guidocument = fcstdfile.open('GuiDocument.xml')
            from xml.dom import minidom
            xmldoc = minidom.parse(guidocument)
            viewproviderlist = xmldoc.getElementsByTagName('ViewProvider')
            for view in viewproviderlist:
                viewpropertylist = view.getElementsByTagName('Property')
                for viewproperty in viewpropertylist:
                    if viewproperty.attributes['name'].value == "Visibility":
                        if viewproperty.getElementsByTagName('Bool')[0].attributes['value'].value == "true":
                            visibleObjectNameList.append(view.attributes['name'].value)
            for name in visibleObjectNameList :
                visibleObjectList.append(doc.getObject(name))
            return visibleObjectList
    if DEBUG :
        print( '... end FreeCADConnect.getVisibleObjects.' )

def getThreeJS(doc):
    tab = "            " # the tab size
    wireframeStyle = "faceloop" # this can be "faceloop", "multimaterial" or None
    cameraPosition = None # set this to a tuple to change, for ex. (0,0,0)
    template = """
            <script>
            
            var container, camera, controls, scene, renderer;
            var SCREEN_WIDTH, SCREEN_HEIGHT


            function onLoad() {
            
                //container
                container = document.getElementById( "webgl" );
                SCREEN_WIDTH = container.clientWidth, SCREEN_HEIGHT =  container.clientHeight;

                //SCENE
                scene = new THREE.Scene();
                scene.background = new THREE.Color( 0xffe4b5 );
                
                //CAMERA
                var VIEW_ANGLE = 35, ASPECT = SCREEN_WIDTH / SCREEN_HEIGHT, NEAR = 0.1, FAR = 20000;
                camera = new THREE.PerspectiveCamera(
                    VIEW_ANGLE,      // Field of view
                    ASPECT,          // Aspect ratio
                    NEAR,            // Near plane
                    FAR              // Far plane
                );
                scene.add(camera);
                $CameraData
                
                // RENDERER
                renderer = new THREE.WebGLRenderer( {antialias:true} );
                renderer.setSize( SCREEN_WIDTH, SCREEN_HEIGHT );
                container.append( renderer.domElement );
                window.addEventListener( 'resize', onWindowResize, false );
                
                // CONTROLS
                controls = new THREE.TrackballControls( camera, container );
                controls.rotateSpeed = 1.0;
                controls.zoomSpeed = 1.2;
                controls.panSpeed = 0.8;
                controls.noZoom = false;
                controls.noPan = false;
                controls.staticMoving = true;
                controls.dynamicDampingFactor = 0.3;
                controls.keys = [ 65, 83, 68 ];
                //GEOM
                $ObjectsData
                // LIGHT
                var light = new THREE.PointLight( 0xFFFF00 );
                light.position.set( -10000, -10000, 10000 );
                scene.add( light );
                
                animate();
                
            };
            
            function onWindowResize() { 
                //container
                container = document.getElementById( "webgl" );
                SCREEN_WIDTH = container.clientWidth, SCREEN_HEIGHT =  container.clientHeight;

                camera.aspect = SCREEN_WIDTH / SCREEN_HEIGHT;
                camera.updateProjectionMatrix();

                renderer.setSize( SCREEN_WIDTH, SCREEN_HEIGHT );
                
            };
            
            function animate(){
            
                requestAnimationFrame( animate );
                controls.update();
                render();
                
            };
            
            function render(){
            
                camera.lookAt( scene.position );
                renderer.render( scene, camera );
                
            };
            </script>"""
            
    objectsList = getVisibleObjects(doc)
    objectsData = ''
    for obj in objectsList:
        objectsData += getObjectData(obj)
    
    t = template.replace("$CameraData",getCameraData(objectsList))
    t = t.replace("$ObjectsData",objectsData)
    return t

def getCameraData( objectsList ):
    "returns the position and direction of the camera as three.js snippet"
    XMin = []
    YMin = []
    ZMin = []
    XMax = []
    YMax = []
    ZMax = []
    for obj in objectsList :
        if obj.isDerivedFrom( "Part::Feature" ):
            if DEBUG : print( obj.Name, " is derived from Part::Feature" )
            XMin.append( obj.Shape.BoundBox.XMin )
            YMin.append( obj.Shape.BoundBox.YMin )
            ZMin.append( obj.Shape.BoundBox.ZMin )
            XMax.append( obj.Shape.BoundBox.XMax )
            YMax.append( obj.Shape.BoundBox.YMax )
            ZMax.append( obj.Shape.BoundBox.ZMax )
        elif obj.isDerivedFrom( "Mesh::Feature" ):
            if DEBUG : print( obj.Name, " is derived from Mesh::Feature" )
            XMin.append( obj.Mesh.BoundBox.XMin )
            YMin.append( obj.Mesh.BoundBox.YMin )
            ZMin.append( obj.Mesh.BoundBox.ZMin )
            XMax.append( obj.Mesh.BoundBox.XMax )
            YMax.append( obj.Mesh.BoundBox.YMax )
            ZMax.append( obj.Mesh.BoundBox.ZMax )
        else :
            if DEBUG : print("Unknown derived from")
    if DEBUG : print("XMax",XMax,"YMax",YMax,"ZMax",ZMax)
    #XMin = min(XMin)
    #YMin = min(YMin)
    #ZMin = min(ZMin)
    XMax = max( XMax ) * 1.2
    YMax = max( YMax ) * 1.2
    ZMax = max( ZMax ) * 1.2
    result = ""
    tab = "            " # the tab size
    # camera position to calculate with bounding box
    result += "camera.position.set(" + str(XMax) + "," + str(YMax) + "," + str(ZMax) +");\n"
    result += tab + "camera.lookAt( scene.position );\n" + tab
    # print result
    return result
   

def getObjectData(obj, wireframeMode="multimaterial"):
    """returns the geometry data of an object as three.js snippet. wireframeMode
    can be multimaterial, faceloop or None"""
    
    result = ""
    tab = "            " # the tab size
    linewidth = 2
    wires = []

    if obj.isDerivedFrom("Part::Feature"):
        fcmesh = obj.Shape.tessellate(0.1)
        result = "var geom = new THREE.Geometry();\n"
        # adding vertices data
        for i in range(len(fcmesh[0])):
            v = fcmesh[0][i]
            result += tab+"var v"+str(i)+" = new THREE.Vector3("+str(v.x)+","+str(v.y)+","+str(v.z)+");\n"
        result += tab+"console.log(geom.vertices)\n"
        for i in range(len(fcmesh[0])):
            result += tab+"geom.vertices.push(v"+str(i)+");\n"
        # adding facets data
        for f in fcmesh[1]:
            result += tab+"geom.faces.push( new THREE.Face3"+str(f)+" );\n"
        for f in obj.Shape.Faces:
            for w in f.Wires:
                wo = Part.Wire(Part.__sortEdges__(w.Edges))
                wires.append(wo.discretize(QuasiDeflection=0.1))

    elif obj.isDerivedFrom("Mesh::Feature"):
        mesh = obj.Mesh
        result = "var geom = new THREE.Geometry();\n"
        # adding vertices data 
        for p in mesh.Points:
            v = p.Vector
            i = p.Index
            result += tab+"var v"+str(i)+" = new THREE.Vector3("+str(v.x)+","+str(v.y)+","+str(v.z)+");\n"
        result += tab+"console.log(geom.vertices)\n"
        for p in mesh.Points:
            result += tab+"geom.vertices.push(v"+str(p.Index)+");\n"
        # adding facets data
        for f in mesh.Facets:
            pointIndices = tuple([ int(i) for i in f.PointIndices ])
            result += tab+"geom.faces.push( new THREE.Face3"+str(pointIndices)+" );\n"
            
    if result:
        # adding a base material
        if FreeCADGui:
            col = obj.ViewObject.ShapeColor
            rgb = Draft.getrgb(col,testbw=False)
        else:
            rgb = "#888888" # test color
        result += tab+"var basematerial = new THREE.MeshBasicMaterial( { color: 0x"+str(rgb)[1:]+" } );\n"
        #result += tab+"var basematerial = new THREE.MeshLambertMaterial( { color: 0x"+str(rgb)[1:]+" } );\n"
        
        if wireframeMode == "faceloop":
            # adding the mesh to the scene with a wireframe copy
            result += tab+"var mesh = new THREE.Mesh( geom, basematerial );\n"
            result += tab+"scene.add( mesh );\n"
            result += tab+"var linematerial = new THREE.LineBasicMaterial({linewidth: %d, color: 0x000000,});\n" % linewidth
            for w in wires:
                result += tab+"var wire = new THREE.Geometry();\n"
                for p in w:
                    result += tab+"wire.vertices.push(new THREE.Vector3("
                    result += str(p.x)+", "+str(p.y)+", "+str(p.z)+"));\n"
                result += tab+"var line = new THREE.Line(wire, linematerial);\n"
                result += tab+"scene.add(line);\n"
            
        elif wireframeMode == "multimaterial":
            # adding a wireframe material
            result += tab+"var wireframe = new THREE.MeshBasicMaterial( { color: "
            result += "0x000000, wireframe: true, transparent: true } );\n"
            result += tab+"var material = [ basematerial, wireframe ];\n"
            result += tab+"var mesh = new THREE.SceneUtils.createMultiMaterialObject( geom, material );\n"
            result += tab+"scene.add( mesh );\n"+tab
            
        else:
            # adding the mesh to the scene with simple material
            result += tab+"var mesh = new THREE.Mesh( geom, basematerial );\n"
            result += tab+"scene.add( mesh );\n"+tab
        
    return result
