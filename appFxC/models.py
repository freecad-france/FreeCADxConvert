# -*- coding: utf-8 -*-

from django.db import models
from django import forms

# Create your models here.

class Document(models.Model):
    docfile = models.FileField(upload_to='input3dfiles')

class DocumentForm(forms.Form):
    docfile = forms.FileField(label='Selectionner un fichier',
                          help_text='Taille max.: 10 megabytes')

"""
class DocumentFreeCAD(models.Model):
    def __init__ (self, input3dfile):
        FreeCAD.loadFile(input3dfile)
        self.doc = FreeCAD.ActiveDocument
        
    def exportWebGl(self):
        __objs__= self.doc.Objects
        #__objs__.append(self.doc.Objects)
        import importWebGL
        importWebGL.export(__objs__,u"/home/jo/Documents/3D/Cube.html")
        import Mesh
        Mesh.export(__objs__,u"/home/jo/Documents/3D/OBJ/Cube.obj")
        del __objs__
"""
