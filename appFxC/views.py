from django.shortcuts import render

# Create your views here.


from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseRedirect
from appFxC.models import Document, DocumentForm
import appFxC.FreeCADConnect as FreeCADConnect

def Index(request):
    """index.html view with a form to upload a file"""
    # create a form
    form = DocumentForm()
    fcversion = "not loaded yet"
    return render(request, 'FxC/index.html',
                    {'fcversion' : fcversion , 'form' : form},
                    )

def Open(request):
    """view when opening the file"""
    # get the FreeCAD version
    fcversion = FreeCADConnect.getFreeCADVersion()
    if request.method == 'POST':
        # form have the requested file
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            # a new Django Document is created
            newdoc = Document(docfile = request.FILES['docfile'])
            # then saved on the disk
            newdoc.save()
            # docpath is /full/path/to/file.ext
            docpath = newdoc.docfile.path
            # doclabel is file.ext
            doclabel = newdoc.docfile.name[13:]
            # docname is the internal name of freecad document 
            docname = FreeCADConnect.load3dfile(docpath, doclabel)
            # we had a key in the session with the internal freecad doc name
            request.session['DocumentFreeCAD'] = docname
            request.session['OriginalPathFile'] = docpath
            # webgl is the return of getThreeJS
            webgl = FreeCADConnect.export3dfile(docname, 'WEBGL')
            # we send a response
            return render(request, 'FxC/preview.html',
                             {'fcversion' : fcversion ,'webgl' : webgl, 'form' : form},
                              )
        else:
            form = DocumentForm()
            return render(request, 'FxC/index.html',
                                 {'fcversion' : fcversion , 'form' : form},
                                  )
    
def Export(request):
    """view when exporting a file"""
    # get the session's FreeCAD document name
    docname = request.session['DocumentFreeCAD']
    # and original file path
    docpath = request.session['OriginalPathFile']
    # need a content type list
    contentTypeDic = { 'FCSTD' : 'application/x-extension-fcstd',
        'STL' : 'application/vnd.ms-pkistl',
        'OBJ' : 'model',
        'STEP' : 'model',
        'IGES' : 'model',
        'AST' : 'model',
        'PLY' : 'model',
        'BMS' : 'model',
        'BREP' : 'model',
        'DAE' : 'model',
        'IFC' : 'model',
        }
    if request.method == 'POST':
        # the value of the choosen format
        extension = request.POST.get("export_format")
        filepath = FreeCADConnect.export3dfile(docname, extension, docpath)
        f = open(filepath)
        response = HttpResponse(f,  content_type = contentTypeDic[extension])
        response['Content-Disposition'] = 'attachment; filename="' + docname + '.' + extension.lower()
        return response

    return render(request, 'FxC/download.html',)
